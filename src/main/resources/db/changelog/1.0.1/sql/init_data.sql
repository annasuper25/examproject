INSERT INTO students (full_name, speciality, course)
VALUES ('Ivanova Alla Ivanovna', 'EUIS', 1),
       ('Smirnov Maksim Evgenievich', 'ISTAS', 2),
       ('Alochkina Larisa Semenovna', 'PGS', 1),
       ('Ivanov Andrey Ivanovich', 'EUIS', 1),
       ('Petrova Polina Dmitrievna', 'ISTAS', 3),
       ('Andreev Nikolay Nikolaevich', 'ISTAS', 4),
       ('Eliseev Konstantin Valerievich', 'EUIS', 3);


INSERT INTO teachers (full_name, department)
VALUES ('Smirnov Daniil Artemovich', 'English Language'),
       ('Kovaleva Maria Nikolaevna', 'Economics'),
       ('Petrova Anna Alekseevna', 'Physica'),
       ('Kopylova Elena Daniilovna', 'History'),
       ('Filipova Marina Dmitrievna', 'Economics'),
       ('Eliseev Andrey Vasilevich', 'PGS'),
       ('Petrov Ivan Vasilevich', 'English Language');

INSERT INTO students_teachers
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (2, 1),
       (2, 4),
       (2, 6),
       (3, 1),
       (3, 2),
       (3, 3),
       (3, 4),
       (4, 1),
       (4, 7),
       (4, 3),
       (4, 4),
       (4, 5),
       (5, 1),
       (5, 4),
       (6, 1),
       (6, 4),
       (6, 7),
       (7, 1),
       (7, 4);

insert into roles VALUES ( nextval('role_id_seq'), 'ROLE_STUDENT'),
                         ( nextval('role_id_seq'), 'ROLE_TEACHER'),
                         ( nextval('role_id_seq'), 'ROLE_ADMIN');

insert into users values
(nextval('user_id_seq'), 'anna', '$2y$12$N8WLw99IyJlEiXSjVZ5RceKdXexEX/GgTm3cP54a44/OYoOtW1LjS'),
(nextval('user_id_seq'), 'admin', '$2y$12$DDx2uYlGdZ/xa/a28BJAVeL.oFQFXE.1BwpUECUnDhdWEimYLY8iy'),
(nextval('user_id_seq'), 'teacherIvan', '$2y$12$/tbGY7u2mI07okjfqfhJYO30b0FOh7.XQPZpnTPwd6eVKuh66SDhO');

insert into users_roles values
(1, 1),
(2, 3),
(3, 2);