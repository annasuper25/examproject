package com.example.college.controller;

import com.example.college.dto.StudentDto;
import com.example.college.dto.TeacherDto;
import com.example.college.service.api.StudentService;
//import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST контроллер для работы со студентами
 */

//@Api(value = "StudentController", description = "Управление информацией о студентах")
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
@Tag(name = "Студент", description = "АПИ для студентов")
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    @Operation(summary = "Получение данных студента по идентификатору")
    public StudentDto getStudentById(@PathVariable Integer id) {
        return studentService.getStudentById(id);
    }

    @GetMapping
    @Operation(summary = "Получение данных всех студентов")
    public List<StudentDto> getAllStudents() {
        return studentService.getAllStudents();
    }


    @PostMapping
    @Operation(summary = "Добавление нового студента")
    public StudentDto addStudent(@RequestBody StudentDto studentDto) {
        return studentService.addStudent(studentDto);
    }


    @PutMapping
    @Operation(summary = "Изменение данных студента")
    public StudentDto updateStudent(@RequestBody StudentDto studentDto) {
        return studentService.updateStudent(studentDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление студента по идентификатору")
    public void deleteStudentById(@PathVariable Integer id) {
        studentService.deleteStudentById(id);
    }


    @PostMapping("/{studentId}/teacher/{teacherId}")
    @Operation(summary = "Приписание студента к преподавателю")
    public void addStudentToTeacher(@PathVariable Integer studentId, @PathVariable Integer teacherId) {
        studentService.addStudentToTeacher(studentId,teacherId);
    }

    @DeleteMapping("/{studentId}/teacher/{teacherId}")
    @Operation(summary = "Отписание студента от преподавателя")
    public void removeStudentFromTeacher(@PathVariable Integer studentId, @PathVariable Integer teacherId){
        studentService.removeStudentFromTeacher(studentId, teacherId);
    }

    @GetMapping("/teachers/{studentId}")
    @Operation(summary = "Получение списка всех преподавателей, у которых учится студент")
    public List<TeacherDto> getTeachersByStudentId(@PathVariable Integer studentId) {
        return studentService.getTeachersByStudentId(studentId);
    }
}
