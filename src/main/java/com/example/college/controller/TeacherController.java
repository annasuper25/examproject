package com.example.college.controller;

import com.example.college.dto.StudentDto;
import com.example.college.dto.TeacherDto;
import com.example.college.service.api.TeacherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST контроллер для работы с учителями
 */

@Tag(name = "Преподаватель", description = "АПИ для преподавателей ")
@RestController
@RequiredArgsConstructor
@RequestMapping("/teachers")
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping
    @Operation(summary = "Получение списка с данными всех преподавателей")
    public List<TeacherDto> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @GetMapping("/{teacherId}")
    @Operation(summary = "Получение данных преподавателя по идентификатору")
    public TeacherDto getTeacherById(@PathVariable Integer teacherId) {
        return teacherService.getTeacherById(teacherId);
    }

    @GetMapping("/students/{teacherId}")
    @Operation(summary = "Получение списка всех студентов, которых обучает преподаватель")
    public List<StudentDto> getStudentsByTeacherId(@PathVariable Integer teacherId) {
        return teacherService.getStudentsByTeacherId(teacherId);
    }

    @PostMapping
    @Operation(summary = "Создание преподавателя")
    public TeacherDto addTeacher(@RequestBody TeacherDto teacherDto) {
        return teacherService.addTeacher(teacherDto);
    }

    @DeleteMapping("/{teacherId}")
    @Operation(summary = "Удаление преподавателя")
    public void deleteTeacher(@PathVariable Integer teacherId) {
        teacherService.deleteTeacher(teacherId);
    }

    @PutMapping
    @Operation(summary = "Изменение данных преподавателя")
    public TeacherDto updateStudent(@RequestBody TeacherDto teacherDto) {
        return teacherService.updateTeacher(teacherDto);
    }
}
