package com.example.college.controller;

import com.example.college.dto.LoginRequestDTO;
import com.example.college.dto.RegistrationRequestDTO;
import com.example.college.entity.User;
import com.example.college.service.api.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * REST контроллер для работы с пользователями
 */

@Tag(name = "Пользователи", description = "АПИ для учетных записей пользователей")
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    @Operation(summary = "Вход в систему по логину и паролю")
    public Authentication login(@RequestBody LoginRequestDTO loginRequestDTO) {
        return authenticationService.authorize(loginRequestDTO);
    }

    @PostMapping("/logout")
    @Operation(summary = "Выход из системы")
    public void logout() {
        authenticationService.logout();
    }


    @PostMapping("/register")
    @Operation(summary = "Регистрация нового пользователя")
    public User register(@RequestBody @Validated RegistrationRequestDTO registrationRequestDTO) {
        return authenticationService.register(registrationRequestDTO);
    }

    @PostMapping("/current")
    @Operation(summary = "Получение данных текущего активного пользователя")
    public Principal current(Principal principal) {
        return principal;
    }


}
