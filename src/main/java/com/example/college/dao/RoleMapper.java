package com.example.college.dao;

import com.example.college.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Mapper
@Repository
public interface RoleMapper {

    @Select("SELECT * FROM ROLES WHERE role_id = #{id}")
    Role getRoleById(Integer id);

    @Select("select r.role_id, r.role_name from roles r join users_roles ur on r.role_id = ur.role_id where ur.user_id = #{userId}")
    Set<Role> getRolesByUserid(int userId);

}
