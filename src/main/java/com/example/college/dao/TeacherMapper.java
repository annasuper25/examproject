package com.example.college.dao;

import com.example.college.entity.Student;
import com.example.college.entity.Teacher;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface TeacherMapper {

    @Select("select * from teachers")
    List<Teacher> getAllTeachers();

    @Select("select teacher_id, full_name, department from teachers " +
            "where teacher_id = #{id}")
    Optional<Teacher> getTeacherById(Integer id);

    @Select("select s.student_id, full_name, speciality, course from students s " +
            "join students_teachers st on s.student_id = st.student_id " +
            "where teacher_id = #{id}")
    List<Student> getStudentsByTeacherId(Integer id);

    @Insert("insert into teachers values (#{teacherId}, #{fullName}, " +
            "#{department})")
    @SelectKey(keyProperty = "teacherId", before = true, resultType = Integer.class,
            statement = "select nextval('teacher_id_seq')")
    void addTeacher(Teacher teacher);

    @Delete("delete from teachers where teacher_id = #{id}")
    void deleteTeacher(Integer id);

    @Delete("delete from students_teachers where teacher_id = #{id}")
    void deleteTeacherFromAllStudents(Integer id);

    @Update("update teachers set full_name = #{fullName}, " +
            "department = #{department} where teacher_id = #{teacherId}")
    void updateTeacher(Teacher teacher);
}
