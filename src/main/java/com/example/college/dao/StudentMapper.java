package com.example.college.dao;

import com.example.college.entity.Student;
import com.example.college.entity.Teacher;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface StudentMapper {

    @Select("SELECT * FROM students")
    List<Student> getAllStudents();

    @Select("SELECT * FROM students WHERE student_id = #{id}")
    Optional<Student> getStudentById(Integer id);

    @Select("SELECT t.teacher_id, full_name, department FROM teachers t\n" +
            " JOIN students_teachers st on t.teacher_id = st.teacher_id WHERE student_id = #{id};")
    List<Teacher> getTeachersByStudentId(Integer id);

    @Insert("insert into students values (#{studentId}, #{fullName}, " +
            "#{speciality}, #{course})")
    @SelectKey(keyProperty = "studentId", before = true, resultType = Integer.class,
            statement = "select nextval('student_id_seq')")
    void addStudent(Student student);

    @Insert("insert into students_teachers values (#{studentId}, #{teacherId})")
    void addStudentToTeacher(Integer studentId, Integer teacherId);

    @Delete("delete from students_teachers where student_id = #{studentId} AND " +
            "teacher_id = #{teacherId}")
    void deleteStudentFromTeacher(Integer studentId, Integer teacherId);

    @Delete("delete from students where student_id = #{id}")
    void deleteStudentById(Integer id);

    @Delete("delete from students_teachers where student_id = #{id}")
    void deleteStudentFromAllTeachers(Integer id);

    @Update("update students set full_name = #{fullName}, " +
            "speciality = #{speciality}, course = #{course} where student_id = #{studentId}")
    void updateStudent(Student student);
}
