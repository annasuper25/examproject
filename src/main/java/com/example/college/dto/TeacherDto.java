package com.example.college.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(title = "Модель данных учителя")
public class TeacherDto {

    @Schema(title = "Идентификатор учителя")
    private Integer teacherId;
    @Schema(title = "ФИО учителя")
    private String fullName;
    @Schema(title = "Кафедра")
    private String department;

}
