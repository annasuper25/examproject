package com.example.college.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Модель запроса на регистрацию
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(title = "Модель данных запроса на регистрацию")
public class RegistrationRequestDTO {
    @NotNull
    @Schema(title = "Имя пользователя (логин)")
    private String username;

    @NotNull
    @Schema(title = "Пароль")
    private String password;

    @Schema(title = "Идентификатор роли")
    private int role_id;

}
