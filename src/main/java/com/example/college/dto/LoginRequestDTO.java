package com.example.college.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Модель запроса на вход в систему
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(title = "Модель данных запроса на вход в систему")
public class LoginRequestDTO {

    @NotNull
    @Schema(title = "Логин пользователя")
    private String login;

    @NotNull
    @Schema(title = "Пароль")
    private String password;
}
