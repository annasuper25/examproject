package com.example.college.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(title = "Модель данных студента")
public class StudentDto {

    @Schema(title = "Идентификатор студента")
    Integer studentId;
    @Schema(title = "ФИО студента")
    String fullName;
    @Schema(title = "Специальность")
    String speciality;
    @Schema(title = "Курс")
    Integer course;
}
