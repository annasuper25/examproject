package com.example.college.service.impl;

import com.example.college.dao.StudentMapper;
import com.example.college.dto.StudentDto;
import com.example.college.dto.TeacherDto;
import com.example.college.entity.Student;
import com.example.college.entity.Teacher;
import com.example.college.service.api.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Реализация сервиса по работе со студентами
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final ModelMapper modelMapper;
    private final StudentMapper studentMapper;


    @Override
    public List<StudentDto> getAllStudents() {
        List<Student> entities = studentMapper.getAllStudents();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public StudentDto getStudentById(int id) {
        Student entity = studentMapper.getStudentById(id).orElseThrow(
                () -> new RuntimeException(String.format("Не найдена сущность студента по идентификатору=%s", id))
        );
        log.debug("Получена сущность студента с id={}", id);
        return modelMapper.map(entity, StudentDto.class);
    }

    @Override
    public List<TeacherDto> getTeachersByStudentId(int id) {
        Student entity = studentMapper.getStudentById(id).orElseThrow(
                () -> new RuntimeException(String.format("Не найдена сущность студента по идентификатору=%s", id))
        );
        List<Teacher> entities = studentMapper.getTeachersByStudentId(id);
        log.info("Получен список преподавателей студента с id={}", id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public StudentDto addStudent(StudentDto studentDto) {
        Student entity = modelMapper.map(studentDto, Student.class);
        studentMapper.addStudent(entity);
        return getStudentById(entity.getStudentId());
    }

    @Override
    public void addStudentToTeacher(int studentId, int teacherId) {
        studentMapper.addStudentToTeacher(studentId, teacherId);
    }

    @Override
    public void removeStudentFromTeacher(int studentId, int teacherId) {
        studentMapper.deleteStudentFromTeacher(studentId, teacherId);
    }

    @Override
    public void deleteStudentById(int id) {
        studentMapper.deleteStudentFromAllTeachers(id);
        studentMapper.deleteStudentById(id);
    }

    @Override
    public StudentDto updateStudent(StudentDto studentDto) {
        Student student = modelMapper.map(studentDto, Student.class);
        studentMapper.updateStudent(student);
        return getStudentById(student.getStudentId());
    }
}
