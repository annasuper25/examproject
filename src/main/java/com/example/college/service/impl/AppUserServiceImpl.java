package com.example.college.service.impl;

import com.example.college.entity.Role;
import com.example.college.entity.User;
import com.example.college.dao.RoleMapper;
import com.example.college.dao.UserMapper;
import com.example.college.service.api.AppUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class AppUserServiceImpl implements AppUserService, UserDetailsService {

    private final UserMapper userMapper;
    private final RoleMapper roleMapper;


    @Override
    public List<User> getAllUsers() {
        return userMapper.getAllUsers();
    }

    @Override
    public User getUserById(int userId) {
        return userMapper.getUserById(userId);
    }

    @Override
    public void addUser(User user) {
        userMapper.addUser(user);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void deleteUser(int userId) {
        userMapper.deleteUser(userId);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws RuntimeException {
        User user =  userMapper.getUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found", username)));
        Set<Role> roles = roleMapper.getRolesByUserid(user.getUserId());
        user.setRoles(roles);
        return user;
    }
}
