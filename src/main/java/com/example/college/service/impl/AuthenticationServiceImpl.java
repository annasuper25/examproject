package com.example.college.service.impl;

import com.example.college.dto.LoginRequestDTO;
import com.example.college.dto.RegistrationRequestDTO;
import com.example.college.entity.Role;
import com.example.college.entity.User;
import com.example.college.dao.RoleMapper;
import com.example.college.dao.UserMapper;
import com.example.college.service.api.AuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final RoleMapper roleMapper;


    @Override
    @Transactional
    public Authentication authorize(LoginRequestDTO loginRequestDTO) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(loginRequestDTO.getLogin(), loginRequestDTO.getPassword());

        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return authentication;
    }

    @Override
    public User register(RegistrationRequestDTO registrationRequestDTO) {
        userMapper.getByLogin(registrationRequestDTO.getUsername())
                .ifPresent((account)-> {
                    throw new RuntimeException(
                            String.format("User with username = %s already exists", registrationRequestDTO.getUsername()));
                });
        log.debug("User with username ={} already exists", registrationRequestDTO.getUsername());
        return createAccount(registrationRequestDTO);
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private User createAccount(RegistrationRequestDTO registrationRequestDTO) {
        User account = new User();
        Set<Role> roles = new HashSet<>();
        int roleId = registrationRequestDTO.getRole_id();

        account.setPassword(passwordEncoder.encode(registrationRequestDTO.getPassword()));
        account.setUsername(registrationRequestDTO.getUsername());

        userMapper.addUser(account);
        userMapper.addRoleToUser(account.getUserId(),roleId);

        Role role = roleMapper.getRoleById(registrationRequestDTO.getRole_id());
        roles.add(role);
        account.setRoles(roles);
        return account;
    }
}
