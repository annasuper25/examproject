package com.example.college.service.impl;

import com.example.college.dao.TeacherMapper;
import com.example.college.dto.StudentDto;
import com.example.college.dto.TeacherDto;
import com.example.college.entity.Student;
import com.example.college.entity.Teacher;
import com.example.college.service.api.TeacherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Реализация сервиса по работе с преподавателями
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class TeacherServiceImpl implements TeacherService {

    private final TeacherMapper teacherMapper;
    private final ModelMapper modelMapper;


    @Override
    public List<TeacherDto> getAllTeachers() {
        List<Teacher> entities = teacherMapper.getAllTeachers();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public TeacherDto getTeacherById(int id) {
        Teacher entity = teacherMapper.getTeacherById(id).orElseThrow(
                () -> new RuntimeException(String.format("Не найдена сущность преподавателя по идентификатору=%s", id))
        );
        log.debug("Получена сущность преподавателя с id={}", id);
        return modelMapper.map(entity, TeacherDto.class);
    }

    @Override
    public List<StudentDto> getStudentsByTeacherId(int id) {
        Teacher teacher = teacherMapper.getTeacherById(id).orElseThrow(
                () -> new RuntimeException(String.format("Не найдена сущность преподавателя по идентификатору=%s", id))
        );
        List<Student> entities = teacherMapper.getStudentsByTeacherId(id);
        log.debug("Получены студенты, которых обучает преподаватель с id={}", id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public TeacherDto addTeacher(TeacherDto teacherDto) {
        Teacher teacher = modelMapper.map(teacherDto, Teacher.class);
        teacherMapper.addTeacher(teacher);
        return getTeacherById(teacher.getTeacherId());
    }

    @Override
    public void deleteTeacher(int id) {
        teacherMapper.deleteTeacherFromAllStudents(id);
        teacherMapper.deleteTeacher(id);
    }

    @Override
    public TeacherDto updateTeacher(TeacherDto teacherDto) {
        Teacher teacher = modelMapper.map(teacherDto, Teacher.class);
        teacherMapper.updateTeacher(teacher);
        return getTeacherById(teacher.getTeacherId());
    }

}
