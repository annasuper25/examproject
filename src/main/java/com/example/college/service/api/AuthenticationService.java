package com.example.college.service.api;


import com.example.college.dto.LoginRequestDTO;
import com.example.college.dto.RegistrationRequestDTO;
import com.example.college.entity.User;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {

    Authentication authorize(LoginRequestDTO loginRequestDTO);

    User register(RegistrationRequestDTO registrationRequestDTO);

    void logout();

}
