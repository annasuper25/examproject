package com.example.college.service.api;

import com.example.college.dto.StudentDto;
import com.example.college.dto.TeacherDto;

import java.util.List;

public interface StudentService {

    List<StudentDto> getAllStudents();

    StudentDto getStudentById(int id);

    List<TeacherDto> getTeachersByStudentId(int id);

    StudentDto addStudent(StudentDto studentDto);

    void addStudentToTeacher(int studentId, int teacherId);

    void removeStudentFromTeacher(int studentId, int teacherId);

    void deleteStudentById(int id);

    StudentDto updateStudent(StudentDto studentDto);
}
