package com.example.college.service.api;

import com.example.college.entity.User;

import java.util.List;

public interface AppUserService {

    List<User> getAllUsers();

    User getUserById(int userId);

    void addUser(User user);

    void updateUser(User user);

    void deleteUser(int userId);
}
