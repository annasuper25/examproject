package com.example.college.service.api;

import com.example.college.dto.StudentDto;
import com.example.college.dto.TeacherDto;

import java.util.List;

public interface TeacherService {

    List<TeacherDto> getAllTeachers();

    TeacherDto getTeacherById(int id);

    List<StudentDto> getStudentsByTeacherId(int id);

    TeacherDto addTeacher(TeacherDto teacherDto);

    void deleteTeacher(int id);

    TeacherDto updateTeacher(TeacherDto teacherDto);
}
